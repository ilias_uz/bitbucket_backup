
def create_new_db(c):
    c.execute('''CREATE TABLE configuration
                    (conf_id INTEGER PRIMARY KEY, config_key text, config_value text)''')
    c.execute('''CREATE TABLE backup
                (hashs text,
                treeroot text,
                rev_id INTEGER,
                FOREIGN KEY (rev_id) REFERENCES revision(rev_id))''')
    c.execute('''CREATE TABLE revision
                (rev_id INTEGER PRIMARY KEY,
                date INTEGER
                )''')


def add_data_in_configuration(conf_id, pat, f, c):
    config = [(conf_id, pat, f),
                  ]
    c.executemany("INSERT INTO configuration VALUES (?, ?, ?)", config)


def add_data_in_backup(hashs, i, rev_id, c):
    backup_dates = [(hashs, i, rev_id),
                            ]
    c.executemany('INSERT INTO backup VALUES (?, ?, ?)', backup_dates)


def add_data_in_revision(row_i, date, c):
    revisions = [(row_i, date),
                             ]
    c.executemany('INSERT INTO revision VALUES (?, ?)', revisions)
