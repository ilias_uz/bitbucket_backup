import hashlib
import os
import fnmatch
import sqlite3

def recursive_glob(treeroot, pattern):
    results = []
    for base, dirs, files in os.walk(treeroot):
        goodfiles = fnmatch.filter(files, pattern)
        results.extend(os.path.join(base, f) for f in goodfiles)
    return results

result = recursive_glob('/home/iliyaz/Documents/', '*.txt')


name_db = 'my_db.db'

if os.path.isfile(name_db):
    conn = sqlite3.connect(name_db)
    c = conn.cursor()

    for i in result:
        d = open(i).read()
        hashs = hashlib.sha1(d).hexdigest()
        backup_dates = [(hashs, i),
                        ]
        for row in c.execute("SELECT hashs, treeroot FROM backup"):
            if row[0] != hashs and row[1] == i:
                show = open(i).read()
                print row, (hashs, i)
            elif row[0] == hashs and row[1] == i:
                print ('Exchanged nothing: %s path: %s' % (row[0], row[1]))
else:
    conn = sqlite3.connect(name_db)
    c = conn.cursor()
    c.execute('''CREATE TABLE backup
                (hashs text, treeroot text)''')
    for i in result:
        d = open(i).read()
        hashs = hashlib.sha1(d).hexdigest()
        backup_dates = [(hashs, i),
                        ]
        c.executemany('INSERT INTO backup VALUES (?, ?)', backup_dates)
    for row in c.execute("SELECT hashs, treeroot FROM backup"):
        print row
    conn.commit()
    conn.close()
