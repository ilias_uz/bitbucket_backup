#! /usr/bin/python
######################################################################
# 
# File: b2_python_pusher
#
# Copyright 2015 Backblaze Inc. All Rights Reserved.
#
# License https://www.backblaze.com/using_b2_code.html
#
######################################################################

import base64
import hashlib
import json
import os
import sys
import urllib
import urllib2
import asyncore, socket

USAGE = """
Usage:

    b2_python_pusher <accountId> <applicationKey> <bucketId> <folderName>

Backs up the current directory, and everything in it, to B2 Cloud
Storage.  

The accountId and accountKey come from your account page at
backblaze.com. 

The bucket to store files in must already exist.  You can use
the B2 command-line tool, b2, to create the bucket.

The folderName is the prefix used for files backed up from the current
folder.   If the folder name you give is "photos/backup" and one of
the files in the current directory is named "kitten.jpg", the
resulting file in B2 will be called "photos/backup/kitten.jpg".
"""

VERSION = '0.3.3'


def b2_url_encode(s):
    """URL-encodes a unicode string to be sent to B2 in an HTTP header.
    """
    return urllib.quote(s.encode('utf-8'))


def b2_url_decode(s):
    """Decodes a Unicode string returned from B2 in an HTTP header.

    Returns a Python unicode string.
    """
    # Use str() to make sure that the input to unquote is a str, not
    # unicode, which ensures that the result is a str, which allows
    # the decoding to work properly.
    return urllib.unquote(str(s)).decode('utf-8')


def fix_backslashes(s):
    """Replaces \ with /.

    This allows Windows paths to work by being translated to use
    the "/" convention that works in B2.  B2 does not allow
    backslashes in file names.
    """
    return s.replace('\\', '/')


def hex_sha1_of_file(path):
    """Returns the SHA1 of the contents of the file at the given path.
    """
    with open(path, 'rb') as f:
        block_size = 1024 * 1024
        digest = hashlib.sha1()
        while True:
            data = f.read(block_size)
            if len(data) == 0:
                break
            digest.update(data)
        return digest.hexdigest()


class OpenUrl(object):
    """
    Context manager that handles an open urllib2.Request, and provides
    the file-like object that is the response.
    """

    def __init__(self, url, data, headers):
        self.url = url
        self.data = data
        self.headers = headers
        self.file = None

    def __enter__(self):
        try:
            request = urllib2.Request(self.url, self.data, self.headers)
            self.file = urllib2.urlopen(request)
            return self.file
        except urllib2.HTTPError as e:
            print 'Error returned from server:'
            print
            print 'URL:', self.url
            print 'Params:', self.data
            print 'Headers:', self.headers
            print
            print e.read()
            raise e

    def __exit__(self, exception_type, exception, traceback):
        if self.file is not None:
            self.file.close()


class HTTPClient(asyncore.dispatcher):

    def __init__(self, host, path):
        asyncore.dispatcher.__init__(self)
        self.create_socket(socket.AF_INET, socket.SOCK_STREAM)
        self.connect( (host, 80) )
        self.buffer = 'GET %s HTTP/1.0\r\n\r\n' % path

    def handle_connect(self):
        pass

    def handle_close(self):
        self.close()

    def handle_read(self):
        print self.recv(8192)

    def writable(self):
        return (len(self.buffer) > 0)

    def handle_write(self):
        sent = self.send(self.buffer)
        self.buffer = self.buffer[sent:]


def post_file(url, headers, file_path):
    """Posts the contents of the file to the URL.

    URL-encodes all of the data in the headers before sending.
    """
    with open(file_path, 'rb') as data_file:
        if 'Content-Length' not in headers:
            headers['Content-Length'] = str(os.path.getsize(file_path))
        encoded_headers = dict(
            (k, b2_url_encode(v))
            for (k, v) in headers.iteritems()
            )
        with OpenUrl(url, data_file, encoded_headers) as response_file:
            json_text = response_file.read()
            return json.loads(json_text)


def make_account_key_auth(account_id, application_key):
    base_64_string = base64.b64encode('%s:%s' % (account_id, application_key))
    return 'Basic ' + base_64_string


class B2Uploader(object):

    """
    Implements just the subset of the B2 api needed to push files
    up to B2.
    """

    AUTH_URL = 'https://api.backblaze.com'

    def __init__(self, account_id, application_key, bucket_id):
        """
        Initializes a new uploader for the given account.
        """
        self.account_id = account_id
        self.application_key = application_key
        self.bucket_id = bucket_id
        self.upload_url = None
        self.upload_auth_token = None

        # Authorize the account
        account_auth = self._call_api(
            self.AUTH_URL,
            '/b2api/v1/b2_authorize_account',
            make_account_key_auth(account_id, application_key),
            {}
            )
        self.account_token = account_auth['authorizationToken']
        self.api_url = account_auth['apiUrl']
        print account_auth

    def hide_file(self, file_name):
        self._call_api(
            self.api_url,
            '/b2api/v1/b2_hide_file',
            self.account_token,
            {'bucketId': self.bucket_id, 'fileName': file_name}
            )
        print 'file name is:' + file_name

    def upload_file(self, local_path, b2_file_name):
        """
        Upload a file.

        If the upload fails, retry after getting a new upload URL.
        """
        for i in xrange(5):
            # If this is the first time, or the last upload failed, get
            # a new upload_url.
            if self.upload_url is None:
                upload_info = self._call_api(
                    self.api_url,
                    '/b2api/v1/b2_get_upload_url',
                    self.account_token,
                    {'bucketId': self.bucket_id}
                )
                self.upload_url = upload_info['uploadUrl']
                self.upload_auth_token = upload_info['authorizationToken']
                print 'got upload url:', self.upload_url
            # Try uploading the file
            try:
                url = self.upload_url
                headers = {
                    'Authorization': self.upload_auth_token,
                    'X-Bz-File-Name': b2_file_name,
                    'Content-Type': 'b2/x-auto',
                    'X-Bz-Content-Sha1': hex_sha1_of_file(local_path)
                }
                file_info = post_file(url, headers, local_path)
                return file_info['fileId']
            except urllib2.HTTPError as e:
                if 500 <= e.code and e.code < 600:
                    self.upload_url = None
                    self.upload_auth_token = None
                else:
                    raise e
        print 'FAILED to upload ' + local_path + ' after 5 tries'
        sys.exit(1)

    def _call_api(self, url_base, api_path, auth_token, request):
        """
        Calls one API by sending JSON and parsing the JSON that comes
        back.
        """
        url = url_base + api_path
        request_data = json.dumps(request)
        headers = {'Authorization': auth_token}
        with OpenUrl(url, request_data, headers) as f:
            json_text = f.read()
            print f
            return json.loads(json_text)


def back_up_directory(base_path, file_names, folder_name, b2_uploader):
    # Get the info on what's already backed up: a map from file name
    # to mod time.
    current_state = {}
    state_file = os.path.join(base_path, '.b2_python_pusher')
    try:
        with open(state_file, 'rb') as f:
            current_state = json.loads(f.read())
    except IOError as e:
        pass

    # Handle deleted files
    # gone = [name for name in current_state.keys() if name not in file_names]
    # for file_name in gone:
    #     local_file_path = base_path + file_name
    #     backup_file_path = fix_backslashes(folder_name + local_file_path)
    #     b2_uploader.hide_file(backup_file_path)
        # print 'deleted: ', b2_url_encode(backup_file_path)

    # Check each of the files to see if they have changed, or are new.
    for file_name in file_names:
        local_file_path = os.path.join(base_path, file_name)
        backup_file_path = fix_backslashes(folder_name + local_file_path)
        mod_time = os.path.getmtime(local_file_path)
        if mod_time != current_state.get(file_name, -1):
            file_id = b2_uploader.upload_file(local_file_path, backup_file_path)
            print file_id, b2_url_encode(folder_name)
            current_state[file_name] = mod_time

    # Save the state
    with open(state_file, 'wb') as f:
        f.write(json.dumps(current_state, indent=4, sort_keys=True))
        f.write('\n')


# def b2_check_files(file_id, account_authorization_token):
#     api_url = 'https://api.backblaze.com'
#     request = urllib2.Request(
#         '%s/b2api/v1/b2_get_file_info' % api_url,
#         json.dumps({'fileId': file_id}),
#         headers={
#             'Authorization': account_authorization_token
#         }
#     )
#     response = urllib2.urlopen(request)
#     response_data = json.loads(response.read())
#     response.close()
#     print response_data
#
#
# def b2_download_fields(bucket_name, file_name, account_authorization_token):
#     download_url = 'https://api.backblaze.com/b2api/v1/b2_authorize_account/'
#     url = download_url + '/file/' + bucket_name + '/' + file_name
#     headers = {
#         'Authorization': account_authorization_token
#     }
#     request = urllib2.Request(url, None, headers)
#     response = urllib2.urlopen(request)
#     response_data = json.loads(response.read())
#     response.close()
#     print response_data

#
# def main():

#
#     if len(sys.argv) != 5:
#         print USAGE
#         sys.exit(1)
#
#     account_id = sys.argv[1]
#     application_key = sys.argv[2]
#     bucket_id = sys.argv[3]
#     folder_name = sys.argv[4]
#
#     # b2_uploader = B2Uploader(account_id, application_key, bucket_id)
#     b2_uploader = B2Uploader(account_id, application_key, bucket_id)
#     # Walk down through all directories, starting at the current one,
#     # backing each one up.
#     #
#     # Passing a unicode string to os.walk() makes it handle unicode
#     # file names better, so that they work on Windows.
#     for (dir_path, dir_names, file_names) in os.walk('./'):
#         if '.b2_python_pusher' in file_names:
#             file_names.remove('.b2_python_pusher')
#         if dir_path == '.':
#             base_path = ''
#         else:
#             assert '.' == os.path.commonprefix([os.path.dirname('./'), dir_path])
#             base_path = dir_path[2:] + '/'
#         back_up_directory(base_path, file_names, folder_name, b2_uploader)
#
# if __name__ == '__main__':
#     main()
