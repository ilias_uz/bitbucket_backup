import hashlib
import sys
import click
import os
import fnmatch
import sqlite3
import shutil
import datetime
from subprocess import Popen, PIPE
import changed_files_functions
import asyncore
from b2_python_pusher import B2Uploader, fix_backslashes, back_up_directory


class Config(object):

    def __init__(self):
        self.verbose = False

pass_config = click.make_pass_decorator(Config, ensure=True)


CONTEXT_SETTINGS = dict(help_option_names=['-h', '--help'])


#invoke_without_command=True,
@click.group(context_settings=CONTEXT_SETTINGS, chain=True)
@click.option('-d', default='database', help='database for configuration and revisions')
@click.option('-f', default='root', help='root of file hierarchy to watch')
@click.option('-pat', default='', help='file type')
@click.option('--verbose', is_flag=True, help='will show more information')
@click.help_option()
@pass_config
def create_db(config, d, f, pat, verbose):
    '''   Backup app for terminal/UNIX in Python.
          This app will check files in directory which directory you want and will be
          write hashs changed date and if you want create revision you can create and
          you can see last your revision after update or create database. All commands
          have help text if you need help you can do that /changed_files -h/ or --help/
    '''
    config.verbose = verbose
    results = []
    rows = []
    result = []
    hashs_i = []
    database = d

    if 'BACKUP_TOOLS_DB' in os.environ:
        database = os.environ['BACKUP_TOOLS_DB']
    config.d = database

    if pat == '':
        pat = "*"

    if os.path.isfile(database):
        conn = sqlite3.connect(database)
        c = conn.cursor()
        for conf in c.execute("SELECT config_value, config_key FROM configuration"):
            f = conf[0]
            pat = conf[1]

    for base, dirs, files in os.walk(f):
        find_file = fnmatch.filter(files, pat)
        results.extend(os.path.join(base, i) for i in find_file)

    if os.path.isfile(database):
        conn = sqlite3.connect(database)
        c = conn.cursor()
        rows_hash = []
        rows_treeroot = []
        result_hash = []


        for row in c.execute("SELECT hashs, treeroot, rev_id FROM backup"):
            rows.append([row[0], row[1]])
            rows_hash.append(row[0])
            rows_treeroot.append(row[1])

        config_key = []
        config_val = []

        for conf in c.execute("SELECT conf_id, config_key, config_value FROM configuration"):
            config_key.append(conf[1])
            config_val.append(conf[2])

        config_id = conf[0]
        config_id += 1

        if pat not in config_key and config_val != f:
            config = [(config_id, pat, f),
                      ]
            c.executemany("INSERT INTO configuration VALUES (?, ?, ?)", config)



        for i in results:
            d = open(i).read()
            hashs = hashlib.sha1(d).hexdigest()
            result.append([hashs, i])
            hashs_i.append(i)
            result_hash.append(hashs)

        rev_i = 0

        for rev in c.execute("SELECT rev_id FROM revision"):
            rev_i = rev[-1]

        rev_i += 1

        for i in result:
            if i not in rows:
                print rows
                backup_dates = [(i[0], i[1], rev_i),
                            ]
                c.executemany('INSERT INTO backup VALUES (?, ?, ?)', backup_dates)

        deleted_treeroot = []

        for i in rows:
            if i[0] == 'deleted':
                deleted_treeroot.append(i[1])

        for i in rows:
            if i[0] == 'deleted' and i[1] not in deleted_treeroot:
                backup_dates = [('deleted', i[1], rev_i),
                                ]
                c.executemany('INSERT INTO backup VALUES (?, ?, ?)', backup_dates)
            elif i[1] not in deleted_treeroot and i[1] not in hashs_i:
                backup_dates = [('deleted', i[1], rev_i),
                                ]
                c.executemany('INSERT INTO backup VALUES (?, ?, ?)', backup_dates)

        conn.commit()
    else:
        conn = sqlite3.connect(database)
        c = conn.cursor()
        c.execute('''CREATE TABLE configuration
                    (conf_id INTEGER PRIMARY KEY, config_key text, config_value text)''')
        c.execute('''CREATE TABLE backup
                    (hashs text,
                    treeroot text,
                    rev_id INTEGER,
                    FOREIGN KEY (rev_id) REFERENCES revision(rev_id))''')
        c.execute('''CREATE TABLE revision
                    (rev_id INTEGER PRIMARY KEY,
                    date INTEGER
                    )''')
        config = [(1, pat, f),
                  ]
        c.executemany("INSERT INTO configuration VALUES (?, ?, ?)", config)

        for i in results:
            d = open(i).read()
            hashs = hashlib.sha1(d).hexdigest()
            backup_dates = [(hashs, i, 1),
                            ]
            c.executemany('INSERT INTO backup VALUES (?, ?, ?)', backup_dates)
        conn.commit()
        conn.close()
    return results


@create_db.command(name='create-revision', short_help='Create a new revision')
@click.pass_context
@pass_config
def create_revision(config):
    if config.verbose:
       print click.ClickException('Error: No such command "verbose".')
    if config.d:
        conn = sqlite3.connect(config.d)
        c = conn.cursor()
        date = datetime.datetime.now()
        if os.path.isfile(config.d):
            row_i = 1
            for row in c.execute("SELECT rev_id FROM backup"):
                row_i = row[-1]
            rev_i = 0
            for rev in c.execute("SELECT rev_id FROM revision"):
                rev_i = rev[-1]
            if rev_i == row_i:
                print "No files have been changed.  Cowardly refusing to create empty revision."
                sys.exit(1)
            if row_i > rev_i:

                # This function will be add a new revision in database
                changed_files_functions.add_data_in_revision(row_i, date, c)
            if row_i < 1:

                # This function will be add a new revision in database
                changed_files_functions.add_data_in_revision(1, date, c)
            conn.commit()


@create_db.command(name='current-revision', short_help='Will be show current revision')
@click.pass_context
@pass_config
def current_revision(config):
    if config.verbose:
        conn = sqlite3.connect(config.d)
        c = conn.cursor()
        row = []
        for row in c.execute("SELECT rev_id, date FROM revision"):
            pass
        print ('rev_id: %s \ntimestamp: %s' % (row[0], row[1]))
    elif config.d:
        conn = sqlite3.connect(config.d)
        c = conn.cursor()
        for row in c.execute("SELECT rev_id FROM revision"):
            pass
        print row[0]


@create_db.command(name='changed-files', short_help='Will be show changed files which you whant')
@click.argument('num')
@click.pass_context
@pass_config
def changed_files(config, num):
    config.num = int(num)
    conn = sqlite3.connect(config.d)
    c = conn.cursor()
    row_i = 0
    for row in c.execute("SELECT hashs, treeroot, rev_id FROM backup"):
        row_i = row[2]
    if row_i < int(num):
        print ("Error: File with this id don't match")
    if config.verbose:
        conn = sqlite3.connect(config.d)
        c = conn.cursor()
        for row in c.execute("SELECT hashs, treeroot, rev_id FROM backup"):
            if row[2] == int(num):
                print ('file_hash: %s \nfile_path: %s \nrev_id: %s' % (row[0], row[1], row[2]))
    elif config.d:
        conn = sqlite3.connect(config.d)
        c = conn.cursor()
        for row in c.execute("SELECT hashs, treeroot, rev_id FROM backup"):
            if row[2] == int(num):
                print ('%s' % (row[1]))


@create_db.command(name='upload-files', short_help='will be upload changed files')
@click.pass_context
@pass_config
def upload_files(config):
    conn = sqlite3.connect(config.d)
    c = conn.cursor()
    for row in c.execute("SELECT rev_id FROM backup"):
        pass
    last_num = row[0]
    for row in c.execute("SELECT hashs, treeroot, rev_id FROM backup"):
        if row[2] == last_num and row[0] != "deleted":
            asyncore.loop()
            base_path_name = os.path.split(row[1])
            base_path = base_path_name[0] + '/'
            rows = [base_path_name[1], ]
            b2_uploader = B2Uploader('a4275764de15', '001002251b54b46c66510b6f5a317051c272bef2f9', '3a34c2971547a6345d3e0115')
            back_up_directory(base_path, rows, 'update', b2_uploader)
            # b2_download_fields('update', row[1], b2_uploader)


@create_db.command(name='deleted-files', short_help='will show deleted files')
@click.pass_context
@click.argument('num', default=1)
@pass_config
def deleted_files(config, num):
    conn = sqlite3.connect(config.d)
    c = conn.cursor()

    for row in c.execute("SELECT hashs, treeroot, rev_id FROM backup"):
        if row[2] == num and row[0] == "deleted":
            print row
        if config.verbose:
            if row[0] == "deleted":
                print row


















