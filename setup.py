from setuptools import setup

setup(
    name='changed_files',
    version='0.1',
    author='Islomzhon Kazikhodzhaev',
    py_modules=['changed_files'],
    install_requires=[
        'Click',
    ],
    entry_points='''
        [console_scripts]
        changed_files=changed_files:create_db
    ''',
)